## meta-mvn

A plugin for [meta](https://github.com/mateodelnorte/meta)

## Use
Runs your mvn command for every directory in your meta project. The order everything is built is the order in which they show up in your .meta file under 'projects'

In the example below, api will be done first, followed by web-service

```json
{
  "projects": {
    "api": "https://git.example.com/me/api",
    "web-service": "https://git.example.com/me/web-service",
  }
}
```

## How to use
```bash
→ meta mvn

  usage:

    meta mvn [options]
```

